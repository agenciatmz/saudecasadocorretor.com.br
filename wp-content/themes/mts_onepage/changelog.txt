v1.1.11 (Apr 21, 2017)
-----------------------------
- Improve contact map accuracy when coordinates are provided
 
Files updated:
- footer.php
- functions.php
- style.css


v1.1.10 (Apr 08, 2017)
 -----------------------------
 - Include Support tab in Theme Options Panel
 - Fixed wrong field constructor args in the Options Panel
 - Load typography scripts only on theme options page
 
 Files updated:
 - style.css
 - functions.php
 - theme-options.php
 - options/support.php
 - options/options.php 
 - options/css/options.css
 - options/js/clipboard.min.js
 - options/js/help.js
 - options/fields/group/field_group.php 
 - options/google-typography/google-typography.php 

v1.1.9 (Apr 07, 2017)
-----------------------------
- Fix notices appearing when no homepage sections are enabled
- Fix javascript errors when the counter homepage section is disabled

Files updated:
- footer.php
- functions.php
- index.php
- js/customscript.js
- style.css

v1.1.8 (Apr 06, 2017)
-----------------------------
- Fixed WooCommerce v3.0.0 backend compatibility issues

Files updated:
- functions.php
- style.css
- options/google-typography/google-typography.php


v1.1.7 (Apr 01, 2017)
-----------------------------
- Improved counter customization. Changing decimal and thousand separator, adding a suffix is now supported for each counter item! Use the filter mts_counter_{i}_args (see home/front-page-counter.php:60).

Files updated:
- functions.php
- home/front-page-counter.php
- js/customscript.js
- js/owl.carousel.min.js
- style.css
- theme-options.php
- js/countUp.min.js

v1.1.6 (Mar 22, 2017)
-----------------------------
- Improved localization

Files updated:
- functions.php
- style.css
- lang/mythemeshop.pot

v1.1.5 (Feb 22, 2017)
-----------------------------
- Fixed: Removed extra HTML markup in homepage slider

Files updated:
- functions.php
- home/front-page-slider.php
- style.css

v1.1.4 (Feb 10, 2017)
-----------------------------
- Fixed: Body scrolls together with the mobile menu

Files updated:
- functions.php
- style.css

v1.1.3 (Jan 30, 2017)
-----------------------------
- Improved homepage posts pagination

Files updated:
- functions.php
- functions/theme-actions.php
- js/customscript.js
- style.css

v1.1.2 (Jan 15, 2017)
-----------------------------
- One click demo importer improvements

Files updated:
- style.css
- functions.php
- options/demo-importer/


v1.1.1 (Oct 22, 2016)
-----------------------------
- One click demo importer improvements
- Install Plugins page improvements

Files updated:
- style.css
- functions.php
- theme-presets.php
- options/
- lang/
- functions/plugin-activation.php


v1.1.0 (Sep 29, 2016)
-----------------------------
- Added one click demo importer
- Updated image upload option field
- Updated language files

Files updated:
- style.css
- functions.php
- theme-presets.php
- options/
- lang/


v1.0.14 (Sep 23, 2016)
-----------------------------
- Updated 'file upload' field in the Theme Options

Files updated:
- style.css
- functions.php
- options/fields/upload/


v1.0.13 (Jul 16, 2016)
-----------------------------
- Added Google Maps API key field in Theme Options - now required by Google
- Close mobile menu on smooth scroll
- Display posts on archive and search pages no matter blog section is not enabled on homepage
- Updated language files

Files updated:
- style.css
- functions.php
- archive.php
- search.php
- footer.php
- theme-options.php
- js/customscript.js
- options/options.php
- lang/


v1.0.12 (Apr 26, 2016)
-----------------------------
- Compatilibity with Wordpress 4.5

Files updated:
- style.css
- functions.php
- js/customscript.js

v1.0.11 (Mar 18, 2016)
-----------------------------
- Updated code on Recommended plugins page

Files updated:
- style.css
- functions/plugin-activation.php
- css/addons.css

v1.0.10 (Mar 16, 2016)
-----------------------------
- Theme folder renamed to "mts_onepage" [You might need to set navigation menus again]
- Added option to control number of Portfolio items on Homepage
- Fixed bug which was showing draft articles in blog section of Homepage
- Fixed few CSS bugs
- Updated Recommended Plugin page design
- Updated default Apple Touch Icon & Gravatar image

Files updated:
- style.css
- functions.php
- theme-options.php
- theme-presets.php
- home/front-page-blog.php
- home/front-page-portfolio.php
- css/addons.css
- functions/plugin-activation.php


v1.0.9 (Feb 8, 2016)
-----------------------------
- Added recommended plugins page

Files changed:
- style.css
- functions/plugin-activation.php
- css/addons.css
- js/addons.js


v1.0.8 (Nov 12, 2015)
-----------------------------
- Changed widget constructors to PHP 5 style
- Fixed false update notification
- Fixed PHP warning on single post
- Fixed twitter section and widget PHP warnings
- Fixed text domain typos
- Updated language files

Files updated:
- style.css
- functions.php
- index.php
- archive.php
- search.php
- single.php
- single-portfolio.php
- page-blog.php
- home/front-page-blog.php
- home/front-page-portfolio.php
- functions/theme-actions.php
- functions/twitteroauth.php
- functions/widget-ad125.php
- functions/widget-ad300.php
- functions/widget-fblikebox.php
- functions/googleplus.php
- functions/widget-social.php
- functions/widget-subscribe.php
- functions/widget-tweets.php


v1.0.7 (July 6th, 2015)
-----------------------------
- Fixed false update notification from wordpress.org

Files updated:
- functions.php
- style.css


v1.0.6 (July 14th, 2014)
-----------------------------
- Fixed Options Panel Tab issue.
- Single Portfolio will show full post content.

Files updated:
- options.js
- theme-actions.php
- style.css


v1.0.4 (June 27th, 2014)
-----------------------------
- Fixed Image alignment & Bullet points issue in single Pages.
- Fixed Double border issue in Tabbed Widget.

Files updated:
- page-contact.php
- page.php
- style.css

v1.0.3 (June 27th, 2014)
-----------------------------
- Fixed footer navigation click issue.
- Added Recommended Slider image size in Options Panel.

Files updated:
- theme-options.php
- style.css

v1.0.2 (June 24th, 2014)
-----------------------------
- Fixed Import issue related to Default Layout.
- Added MyThemeShop Connect plugin in Recommended plugins section.

Files updated:
- plugin-activation.php
- theme-presets.php
- style.css

v1.0.1 (June 24th, 2014)
-----------------------------
- Fixed Traditional blog layout bug on homepage.

Files updated:
- front-page-blog.php
- style.css

v1.0 (June 24th, 2014)
----------------------------
- Theme released
